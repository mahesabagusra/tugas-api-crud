const Product = require("./domain");
const Mysql = require("../../../../helpers/databases/mysql/db");
const config = require("../../../../infra/configs/global_config");
const db = new Mysql(config.get("/mysqlConfig"));

const insertProduct = async (payload) => {
  const product = new Product(db);
  const postCommand = async (payload) => product.create(payload);
  return postCommand(payload);
};

const deleteProduct = async (id) => {
  const product = new Product(db);
  const postCommand = async (id) => product.delete(id);
  return postCommand(id);
};

const UpdateProduct = async (payload, id) => {
  const product = new Product(db);
  const postCommand = async (payload, id) => product.update(payload, id);
  return postCommand(payload, id);
};


module.exports = {
  insertProduct,
  deleteProduct,
  UpdateProduct,
};
